/* globals describe, it, beforeAll */
'use strict';
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

describe('generator-jscad:app', () => {
  beforeAll(() => {
    return helpers.run(path.join(__dirname, '../generators/app')).withPrompts({
      name: 'unit-test',
      description: 'desc for unit test project',
      author: 'Unit Test'
    });
  });

  it('creates files', () => {
    assert.file([
      'package.json',
      '.eslintrc.js',
      'package.json',
      'gulpfile.js',
      'README.md',
      '.gitignore',
      '.gitlab-ci.yml',
      '.vuepress/config.js'
    ]);
  });
});
