'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const path = require('path');
const camelCase = require('lodash/camelCase');
const commandExists = require('command-exists').sync;

module.exports = class extends Generator {
  initializing() {
    this.gitc = {
      user: {
        name: this.user.git.name(),
        email: this.user.git.email()
      }
    };
  }

  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the riveting ${chalk.red('generator-jscad')} generator!`
      )
    );

    const prompts = [
      {
        name: 'name',
        message: 'Your project name',
        default: this.config.get('name') || path.basename(process.cwd())
      },
      {
        name: 'description',
        message: 'Description',
        default: this.config.get('description')
      },
      {
        name: 'projectType',
        message: 'What type of project?',
        default: this.config.get('projectType'),
        type: 'list',
        choices: [
          { name: 'single part', value: 'single' },
          { name: 'multiple parts choose with a list', value: 'multi-list' },
          {
            name: 'multiple parts choose with a checkbox',
            value: 'multi-checkbox'
          }
        ]
      },
      {
        name: 'author',
        message: 'Author',
        default: this.config.get('author') || this.gitc.user.name
      },
      {
        name: 'homepage',
        message: 'Do you have a homepage?',
        default: this.config.get('homepage') || 'http://github.com'
      }
    ];

    return this.prompt(prompts).then((props) => {
      // To access props later use this.props.someAnswer;
      this.props = props;

      this.config.set('name', this.props.name);
      this.config.set('description', this.props.description);
      this.config.set('projectType', this.props.projectType);
      this.config.set('author', this.props.author);
      this.config.set('homepage', this.props.homepage);
    });
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('gulpfile.js'),
      this.destinationPath('gulpfile.js'),
      {
        name: this.props.name,
        description: this.props.description,
        author: this.props.author,
        nameslug: camelCase(this.props.name)
      }
    );

    this.fs.copy(
      this.templatePath('_eslintrc.js'),
      this.destinationPath('.eslintrc.js')
    );

    this.fs.copy(
      this.templatePath('_gitignore'),
      this.destinationPath('.gitignore')
    );

    this.fs.copyTpl(
      this.templatePath('package.json'),
      this.destinationPath('package.json'),
      {
        name: this.props.name,
        description: this.props.description,
        author: this.props.author,
        nameslug: camelCase(this.props.name),
        homepage: this.props.homepage
      }
    );

    this.fs.copyTpl(
      this.templatePath('README.md'),
      this.destinationPath('README.md'),
      {
        name: this.props.name,
        description: this.props.description,
        author: this.props.author,
        nameslug: camelCase(this.props.name),
        homepage: this.props.homepage
      }
    );

    /**
     * Do not overwrite the main code file.
     */
    if (!this.fs.exists(this.props.name + '.jscad')) {
      var template =
        this.props.projectType == 'single'
          ? 'main.jscad'
          : this.props.projectType == 'multi-list'
          ? 'main-list.jscad'
          : 'main-checkbox.jscad';
      this.fs.copyTpl(
        this.templatePath(template),
        this.destinationPath(this.props.name + '.jscad'),
        {
          name: this.props.name,
          description: this.props.description,
          author: this.props.author,
          nameslug: camelCase(this.props.name),
          homepage: this.props.homepage
        }
      );
    }

    this.fs.copy(
      this.templatePath('_vuepress/components/VuepressOpenJscad.vue'),
      this.destinationPath('.vuepress/components/VuepressOpenJscad.vue')
    );

    this.fs.copy(
      this.templatePath('_vuepress/config.js'),
      this.destinationPath('.vuepress/config.js')
    );

    this.fs.copy(
      this.templatePath('_gitlab-ci.yml'),
      this.destinationPath('.gitlab-ci.yml')
    );

    this.fs.copy(
      this.templatePath('prettier.config.js'),
      this.destinationPath('prettier.config.js')
    );
  }

  install() {
    this.npmInstall();

    if (commandExists('git')) {
      this.composeWith(require.resolve('generator-git-init'), {});
    }
  }

  end() {
    this.log(`

jscad generator is finished.

Available scripts:

Bundle the '${this.props.name + '.jscad'}' file into the 'dist' directory.
npm start

Run the development VuePress site.
npm run serve

Build the 'dist' directory and the VuePress site.
npm run build

`);
  }
};
