## 2.2.0 (2020-09-13)

* Add options for multiple part projects ([63005a5](https://gitlab.com/johnwebbcole/generator-jscad/commit/63005a5))
* Updating readme ([ed33447](https://gitlab.com/johnwebbcole/generator-jscad/commit/ed33447))



## <small>2.1.2 (2019-01-12)</small>

* 2.1.2 ([44051d1](https://gitlab.com/johnwebbcole/generator-jscad/commit/44051d1))
* cicd changes ([decc610](https://gitlab.com/johnwebbcole/generator-jscad/commit/decc610))



## <small>2.1.1 (2019-01-12)</small>

* 2.1.1 ([0461e7c](https://gitlab.com/johnwebbcole/generator-jscad/commit/0461e7c))
* update watch to match v4 ([05d96f1](https://gitlab.com/johnwebbcole/generator-jscad/commit/05d96f1))



## 2.1.0 (2019-01-06)

* 2.1.0 ([d06e677](https://gitlab.com/johnwebbcole/generator-jscad/commit/d06e677))
* add vuepress and vue-openjscad ([5204f44](https://gitlab.com/johnwebbcole/generator-jscad/commit/5204f44))
* add vuepress and vue-openjscad ([dac022b](https://gitlab.com/johnwebbcole/generator-jscad/commit/dac022b))



## <small>2.0.2 (2018-12-24)</small>

* 2.0.2 ([7203c0e](https://gitlab.com/johnwebbcole/generator-jscad/commit/7203c0e))
* renamed dot template files ([d23e7a0](https://gitlab.com/johnwebbcole/generator-jscad/commit/d23e7a0))



## <small>2.0.1 (2018-12-24)</small>

* 2.0.1 ([5208a2b](https://gitlab.com/johnwebbcole/generator-jscad/commit/5208a2b))
* add lodash ([e53a8f0](https://gitlab.com/johnwebbcole/generator-jscad/commit/e53a8f0))



## 2.0.0 (2018-12-24)

* 2.0.0 ([a805277](https://gitlab.com/johnwebbcole/generator-jscad/commit/a805277))
* add cicd ([d341b77](https://gitlab.com/johnwebbcole/generator-jscad/commit/d341b77))
* add release script ([e90810a](https://gitlab.com/johnwebbcole/generator-jscad/commit/e90810a))
* Added `node_modules` to watch list ([20d72d0](https://gitlab.com/johnwebbcole/generator-jscad/commit/20d72d0))
* Added readme to generator and moved one into the project.  Camel cased the name to make it a valid o ([66880e6](https://gitlab.com/johnwebbcole/generator-jscad/commit/66880e6))
* added yarn files ([8c96cff](https://gitlab.com/johnwebbcole/generator-jscad/commit/8c96cff))
* changed to use new gulp-jscadfiles plugin ([567183f](https://gitlab.com/johnwebbcole/generator-jscad/commit/567183f))
* Fix gulp file, it needed to be a template. ([ccab77e](https://gitlab.com/johnwebbcole/generator-jscad/commit/ccab77e))
* Initial commit ([e43fb5b](https://gitlab.com/johnwebbcole/generator-jscad/commit/e43fb5b))
* no message ([857331c](https://gitlab.com/johnwebbcole/generator-jscad/commit/857331c))
* no message ([9572114](https://gitlab.com/johnwebbcole/generator-jscad/commit/9572114))
* no message ([bb05b43](https://gitlab.com/johnwebbcole/generator-jscad/commit/bb05b43))
* publish 1.0.1 ([8c26637](https://gitlab.com/johnwebbcole/generator-jscad/commit/8c26637))
* Rebuit using `generator-generator`. ([d7ea44e](https://gitlab.com/johnwebbcole/generator-jscad/commit/d7ea44e))
* removed fileExists dependency ([a239839](https://gitlab.com/johnwebbcole/generator-jscad/commit/a239839))
* reset version ([0932c0e](https://gitlab.com/johnwebbcole/generator-jscad/commit/0932c0e))
* updated dependencies ([93c19df](https://gitlab.com/johnwebbcole/generator-jscad/commit/93c19df))
* updated example, added readme from generator. ([b0df213](https://gitlab.com/johnwebbcole/generator-jscad/commit/b0df213))
* updated to use newer utils ([17b81ea](https://gitlab.com/johnwebbcole/generator-jscad/commit/17b81ea))
* Updating to use `jscad-utils` 2.0 ([7ca7621](https://gitlab.com/johnwebbcole/generator-jscad/commit/7ca7621))



